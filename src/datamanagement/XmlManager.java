package datamanagement;

import java.io.FileWriter;
import java.io.IOException;

import org.jdom.Document;
import org.jdom.input.SAXBuilder;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;
import org.jdom.JDOMException;

public class XmlManager 
{
    private static XmlManager self_ = null;
    private Document          document_;
    
        
    
    private XmlManager() 
    {
        initialise();
    }

    
    
    public void initialise() 
    {
        String string = AppProperties.getInstance().getProperties().
                        getProperty("XMLFILE");
        try {
            SAXBuilder saxBuilder = new SAXBuilder();
            saxBuilder.setExpandEntities(true);
            document_ = saxBuilder.build(string);
        }
        catch (JDOMException exception) {
            System.err.printf("%s", "DBMD: XMLManager : init : " +
                              "caught JDOMException\n");
            throw new RuntimeException("DBMD: XMLManager : init : " +
                                       "JDOMException");    
        } 
        catch (IOException exception) {
            System.err.printf("%s", "DBMD: XMLManager : init : " +
                              "caught IOException\n");
            throw new RuntimeException("DBMD: XMLManager : init : " +
                                       "IOException");
        }
    }

    
    
    public static XmlManager getInstance() 
    { 
        if (self_ == null ) {
            self_ =  new XmlManager(); 
        }
        return self_;
    }
    
    
    
    public Document getDocument()
    {
        return document_;
    }
    
    
    
    public void saveDocument() 
    {
        String xmlFile = AppProperties.getInstance().getProperties().
                         getProperty("XMLFILE");
        
        try (FileWriter fileWriter = new FileWriter(xmlFile)) {
            XMLOutputter xmlOutputter = new XMLOutputter(Format.
                                                         getPrettyFormat());
            xmlOutputter.output(document_, fileWriter);
            fileWriter.close();
        }
        catch (IOException exception) {
            System.err.printf("%s\n", "DBMD : XMLManager : saveDocument : " +
                              "Error saving XML to " + xmlFile);
            throw new RuntimeException("DBMD: XMLManager : saveDocument : " +
                                       "error writing to file");
        }
    }
}
