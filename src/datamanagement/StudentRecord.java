package datamanagement;

public class StudentRecord 
implements StudentRecordInterface 
{
    private Integer studentId_;
    private String unitCode_;
    private float asg1Mark_, asg2Mark_, examMark_;

    

    public StudentRecord(Integer studentId, String unitCode, float asg1Mark, 
                         float asg2Mark, float examMark) 
    {
        studentId_ = studentId;
        unitCode_ = unitCode;
        
        setAsg1Mark(asg1Mark);
        setAsg2Mark(asg2Mark);
        setExamMark(examMark);
    }

    
    
    public Integer getStudentId() 
    {
        return studentId_;
    }

    
    
    public String getUnitCode() 
    {
        return unitCode_;
    }

    
    
    public void setAsg1Mark(float asg1Mark) 
    {
        int asg1Weight = UnitManager.getInstance().
                         getUnit(unitCode_).getAsg1Weight();
        
        if (asg1Mark < 0 || asg1Mark > asg1Weight) {                                                                            
            throw new RuntimeException("Mark cannot be less than zero or " + 
                                       "greater than assessment weight");
        }
        asg1Mark_ = asg1Mark;
    }

    
    
    public float getAsg1Mark() 
    {
        return asg1Mark_;
    }

    
    
    public void setAsg2Mark(float asg2Mark) 
    {
        int asg2Weight = UnitManager.getInstance().
                         getUnit(unitCode_).getAsg2Weight();
        
        if (asg2Mark < 0 || asg2Mark > asg2Weight) {
            throw new RuntimeException("Mark cannot be less than zero or " +
                                       "greater than assessment weight");
        }
        asg2Mark_ = asg2Mark;
    }

    
    
    public float getAsg2Mark() 
    {
        return asg2Mark_;
    }

    
    
    public void setExamMark(float examMark) 
    {
        int examWeight = UnitManager.getInstance().
                         getUnit(unitCode_).getExamWeight();
        
        if (examMark < 0 || examMark > examWeight) {
            throw new RuntimeException("Mark cannot be less than zero or " +
                                       "greater than assessment weight");
        }
        examMark_ = examMark;
    }

    
    
    public float getExamMark() 
    {
        return examMark_;
    }

    
    
    public float computeTotalMark() 
    {
        return asg1Mark_ + asg2Mark_ + examMark_;
    }
}
