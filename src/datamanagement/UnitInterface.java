package datamanagement;

public interface UnitInterface 
{

	
	
    public String getUnitCode();
    
    
    
    public String getUnitName();
    
    

    public float getPassCutOff();
    
    
    
    public void  setPassCutOff(float cutOff);

    
    
    public float getCreditCutOff();
    
    
    
    public void  setCreditCutOff(float cutOff);

    
    
    public float getDistinctionCutOff(); 
    
    
    
    public void  setDistinctionCutOff(float cutOff);
    
    

    public float getHighDistinctionCutOff();
    
    
    
    public void  setHighDistinctionCutOff(float cutOff);  
    
    

    public float getAdditionalExamCutOff();
    
    
    
    public void  setAdditionalExamCutOff(float cutOff);
    
    
    
    public int getAsg1Weight();
    
    
    
    public int getAsg2Weight();
    
    
    
    public int getExamWeight();
    
    
    
    public void setAsgWeights(int asg1Weight, int asg2Weight, int examWeight);
    
    

    public String getGrade(float asg1, float asg2, float exam);

    
    
    public void addStudentRecord(StudentRecordInterface record);
    
    
    
    public StudentRecordInterface getStudentRecord(int studentId);
    
    
    
    public StudentRecordList getStudentRecords();
}