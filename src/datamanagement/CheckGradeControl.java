package datamanagement;

public class CheckGradeControl 
{
    private CheckGradeUi checkGradeUi_; 
    private String       unitCode_;
    private Integer      studentId_;
    private boolean      marksChanged_;

    
    
    public CheckGradeControl()
    {
        unitCode_     = null;
        studentId_    = null;
        marksChanged_ = false;      
    }

    
    
    public void execute() 
    {
        checkGradeUi_ = new CheckGradeUi(this);
        
        checkGradeUi_.enableUnitComboBox(false);
        checkGradeUi_.enableStudentComboBox(false);
        checkGradeUi_.enableCheckGradeBtn(false);
        checkGradeUi_.enableChangeMarksBtn(false);
        checkGradeUi_.enableMarkEditing(false);
        checkGradeUi_.enableSaveRecordBtn(false);
        checkGradeUi_.resetTextFields(); 

        ListUnitsControl listUnitsControl = new ListUnitsControl();
        listUnitsControl.listUnits(checkGradeUi_);
        
        checkGradeUi_.setVisible(true);
        checkGradeUi_.enableUnitComboBox(true);
    }

    
    
    public void unitSelected(String unitCode)
    {
        boolean isNone = unitCode.equals("NONE");
        if (isNone) {
            checkGradeUi_.enableStudentComboBox(false);
        }
        else {
            ListStudentsControl listStudentsControl = new ListStudentsControl();
            listStudentsControl.listStudents(checkGradeUi_, unitCode);
            unitCode_ = unitCode;
            checkGradeUi_.enableStudentComboBox(true);
        }
        
        checkGradeUi_.enableCheckGradeBtn(false);
    }

    
    
    public void studentSelected(Integer studentId)
    {
        studentId_ = studentId;
        boolean studentIdIsZero = studentId_.intValue() == 0;
        
        if (studentIdIsZero) {
            checkGradeUi_.resetTextFields();
            checkGradeUi_.enableCheckGradeBtn(false);
            checkGradeUi_.enableChangeMarksBtn(false);
            checkGradeUi_.enableMarkEditing(false);
            checkGradeUi_.enableSaveRecordBtn(false);
        }
        else {
            StudentInterface student = StudentManager.getInstance().
                                       getStudent(studentId);

            StudentRecordInterface record = student.getRecordByUnit(unitCode_);

            checkGradeUi_.setRecordForDisplay(record);
            checkGradeUi_.enableCheckGradeBtn(true);
            checkGradeUi_.enableChangeMarksBtn(true);
            checkGradeUi_.enableMarkEditing(false);
            checkGradeUi_.enableSaveRecordBtn(false);
            marksChanged_ = false;
        }
    }

    
    
    public String checkGrade(float asg1Mark, float asg2Mark, float examMark) 
    {
        UnitInterface unit = UnitManager.getInstance().getUnit(unitCode_);
        String grade = unit.getGrade(asg1Mark, asg2Mark, examMark); 
        checkGradeUi_.enableChangeMarksBtn(true);
        checkGradeUi_.enableMarkEditing(false);
        if (marksChanged_) {
            checkGradeUi_.enableSaveRecordBtn(true);
        }
        return grade;
    }

    
    
    public void enableChangeMarks() 
    {
        checkGradeUi_.enableChangeMarksBtn(false);
        checkGradeUi_.enableSaveRecordBtn(false);
        checkGradeUi_.enableMarkEditing(true);
        marksChanged_ = true;
    }

    
    
    public void saveGrade(float asg1, float asg2, float exam) 
    {
        StudentInterface student = StudentManager.getInstance().
                                   getStudent(studentId_);
        
        StudentRecordInterface record = student.getRecordByUnit(unitCode_);
        record.setAsg1Mark(asg1);
        record.setAsg1Mark(asg2);
        record.setExamMark(exam);
        StudentRecordManager.getInstance().saveRecord(record);
        checkGradeUi_.enableChangeMarksBtn(true);
        checkGradeUi_.enableMarkEditing(false);
        checkGradeUi_.enableSaveRecordBtn(false);
    }
}
