package datamanagement;

public interface StudentInterface {

    
    
    public Integer getStudentId();

    
    
    public String getFirstName();
    
    
    
    public void setFirstName(String firstName);

    
    
    public String getLastName();
    
    
    
    public void setLastName(String lastName);
    
    
    
    public void addRecord(StudentRecordInterface record );
    
    
    
    public StudentRecordInterface getRecordByUnit(String unitCode);

    
    
    public StudentRecordList getRecords();
}
