package datamanagement;

public interface StudentListerInterface 
{
    
    
    
    public void clearStudents();
    
    
    
    public void addStudent(StudentInterface student);
}