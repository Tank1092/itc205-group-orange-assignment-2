package datamanagement;

import java.io.FileInputStream;
import java.io.IOException;

import java.util.Properties;

public class AppProperties 
{       
    private static AppProperties self_ = null;
    private Properties           properties_;

    
    
    private AppProperties() 
    {
        properties_ = new Properties();
        try {
            properties_.load(new FileInputStream("Properties.prop"));
        } 
        catch (IOException ioException) {
            throw new RuntimeException("Could not read property file");
        }
    }
    
    
    
    public static AppProperties getInstance() 
    {
        boolean AppPropertiesIsNull = self_ == null;
        if (AppPropertiesIsNull) { 
            self_ = new AppProperties(); 
        } 
        return self_;
    }
    
    
    
    public Properties getProperties() 
    {
        return properties_; 
    }   
}
