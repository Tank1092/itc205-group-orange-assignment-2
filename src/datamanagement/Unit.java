package datamanagement;

public class Unit 
implements UnitInterface 
{	
    private String unitCode_;
    private String unitName_;
    private float  passCutOff_;
    private float  creditCutOff_;
    private float  distinctionCutOff_;
    private float  highDistinctionCutOff_;
    private float  additionalExamCutOff_;
    private int    asg1Weight_, asg2Weight_, examWeight_;
    private StudentRecordList studentRecordList_;

    
 
    public Unit(String unitCode, String unitName, float passCutOff,
    		    float creditCutOff, float distinctionCutOff, 
                float highDistinctionCutOff, float additionalExamCutOff, 
                int asg1Weight, int asg2Weight, 
                int examWeight, StudentRecordList studentRecordList) 
    {
        unitCode_              = unitCode;
        unitName_              = unitName;
        passCutOff_            = passCutOff;
        creditCutOff_          = creditCutOff;
        distinctionCutOff_     = distinctionCutOff;
        highDistinctionCutOff_ = highDistinctionCutOff;
        additionalExamCutOff_  = additionalExamCutOff;

        setAsgWeights(asg1Weight, asg2Weight, examWeight);
        
        if (studentRecordList == null) {
        	studentRecordList_ = new StudentRecordList();
        }
        else {
        	studentRecordList_ = studentRecordList;
        }
    }
    
    

    public String getUnitCode() 
    {
        return unitCode_;
    }

    
    
    public String getUnitName() 
    {
        return unitName_;
    }
    
    

    public void setPassCutOff(float cutOff) 
    {
        passCutOff_ = cutOff;
    }
    
    

    public float getPassCutOff() 
    {
        return passCutOff_;
    }
    
    

    public void setCreditCutOff(float cutOff) 
    {
        creditCutOff_ = cutOff;
    }
    
    

    public float getCreditCutOff() 
    {
        return creditCutOff_;
    }
    
    

    public void setDistinctionCutOff(float cutOff) 
    {
        distinctionCutOff_ = cutOff;
    }
    
    

    public float getDistinctionCutOff() 
    {
        return distinctionCutOff_;
    }
    
    

    public void setHighDistinctionCutOff(float cutOff) 
    {
        highDistinctionCutOff_ = cutOff;
    }

    
    
    public float getHighDistinctionCutOff() 
    {
        return highDistinctionCutOff_;
    }
    
    

    public void setAdditionalExamCutOff(float cutOff) 
    {
        additionalExamCutOff_ = cutOff;
    }
    
    

    public float getAdditionalExamCutOff() 
    {
        return additionalExamCutOff_;
    }
    
    

    public void addStudentRecord(StudentRecordInterface record) 
    {
        studentRecordList_.add(record);
    }
    
    
   
    public StudentRecordInterface getStudentRecord(int studentId) 
    {
        for (StudentRecordInterface recordInterface : studentRecordList_) {
        	boolean isRecordInterface = (recordInterface.getStudentId() == 
        			                     studentId);
        	
            if (isRecordInterface) {
                return recordInterface;
            }
        }
        return null;
    }
    
    

    public StudentRecordList getStudentRecords() 
    {
        return studentRecordList_;
    }
    
    

    @Override
    public int getAsg1Weight() 
    {
        return asg1Weight_;
    }
    
    

    @Override
    public int getAsg2Weight() 
    {
        return asg2Weight_;
    }
    
    

    @Override
    public int getExamWeight() 
    {
        return examWeight_;
    }
    
    

    @Override
    public void setAsgWeights(int asg1Weight, int asg2Weight, int examWeight) 
    {
        boolean isWeight1OutRange    = (asg1Weight < 0 || asg1Weight > 100);
        boolean isWeight2OutRange    = (asg2Weight < 0 || asg2Weight > 100);
        boolean isWeightExamOutRange = (examWeight < 0 || examWeight > 100);
        int totalMark                = asg1Weight + asg2Weight + examWeight;
        
        if (isWeight1OutRange || isWeight2OutRange || isWeightExamOutRange) {
            throw new RuntimeException("Assessment weights cannot be less " + 
                                       "than zero or greater than 100");
        }
        else if (totalMark != 100) {
            throw new RuntimeException("Assessment weights must add to 100");
        }
        else {
            asg1Weight_ = asg1Weight;
            asg2Weight_ = asg2Weight;
            this.examWeight_ = examWeight;
        }
    }
    
    
    
    private void setCutOffs(float pass, float credit, float distinction, 
    		                float highDistinction, float additionalExam) 
    {
        boolean isPassCutOffInRange = (pass < 0 || pass > 100);
        boolean isCreditCutOffInRange = (credit < 0 || credit > 100);
        boolean isDistinctionCutOffInRange = (distinction < 0 || 
        		                              distinction > 100);
        boolean isHighDistinctionCutOffInRange = (highDistinction < 0 ||
        		                                  highDistinction > 100);
        boolean isAdditionalExanCutOffInRange = (additionalExam < 0 || 
        		                                 additionalExam > 100);
        
        if (isPassCutOffInRange || isCreditCutOffInRange || 
        	isDistinctionCutOffInRange || isHighDistinctionCutOffInRange ||
        	isAdditionalExanCutOffInRange ) { 
            throw new RuntimeException("Assessment cutoffs cannot be less " +
            		                   "than zero or greater than 100");
        }
        
        if (additionalExam >= pass) {
            throw new RuntimeException("Additional Exam cutoff must be less " +
                                       "than Pass cut off");
        }
        
        if (pass >= credit) {
            throw new RuntimeException("Pass cutoff must be less than Credit " +
                                       "cut off");
        }
        
        if (credit >= distinction) {
            throw new RuntimeException("Credit cutoff must be less than " +
                                       "Distinction cut off");
        }
        
        if (distinction >= highDistinction) {
            throw new RuntimeException("Distinction cutoff must be less than " +
                                       "High Distinction cut off");
        }
    }
    
    
    
    public String getGrade(float assignment1, float assignment2, float exam) 
    {
        float totalMark = assignment1 + assignment2 + exam;
        boolean isAssignment1OutRange = (assignment1 < 0 || assignment1 >
                                         asg1Weight_);
        boolean isAssignment2OutRange = (assignment2 < 0 || assignment2 > 
                                         asg2Weight_);
        boolean isExamOutRange = (exam < 0 || exam > examWeight_);
        
        if (isAssignment1OutRange || isAssignment2OutRange || isExamOutRange) {
            throw new RuntimeException("Marks cannot be less than zero or " +
                                       "greater than assessment weights");
        }

        if (totalMark < additionalExamCutOff_) {
            return "Fail";
        }
        else if (totalMark < passCutOff_) {
            return "Additional Exam";
        }
        else if (totalMark < creditCutOff_) {
            return "Pass";
        }
        else if (totalMark < distinctionCutOff_) {
            return "Credit";
        }
        else if (totalMark < highDistinctionCutOff_){
            return "Distinction";
        }
        else {
            return "High Distinction";
        }
    }
}