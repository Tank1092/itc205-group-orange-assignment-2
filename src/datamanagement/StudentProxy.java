package datamanagement;

public class StudentProxy 
implements StudentInterface 
{   
    private Integer        studentId_;
    private String         firstName_;
    private String         lastName_;
    private StudentManager studentManager_;
    
    
    
    public StudentProxy(Integer studentId, String firstName, String lastName)
    {
        studentId_      = studentId;
        firstName_      = firstName;
        lastName_       = lastName;
        studentManager_ = StudentManager.getInstance();
    }
    
    
    
    public Integer getStudentId() 
    { 
        return studentId_; 
    }
    
    
    
    public String getFirstName()
    { 
        return firstName_; 
    }

    
    
    public String getLastName()
    { 
        return lastName_; 
    }
    
    
    
    public void setFirstName(String firstName)
    {
        studentManager_.getStudent(studentId_).setFirstName(firstName);
    }
    
    
    
    public void setLastName(String lastName)
    {
        studentManager_.getStudent(studentId_).setLastName(lastName);
    }



    public void addRecord(StudentRecordInterface record) 
    {
        studentManager_.getStudent(studentId_).addRecord(record);
    }
    
    
    
    public StudentRecordInterface getRecordByUnit(String unitCode) 
    {
        return studentManager_.getStudent(studentId_).getRecordByUnit(unitCode);
    }



    public StudentRecordList getRecords() 
    { 
        return studentManager_.getStudent(studentId_).getRecords();
    }
}
