package datamanagement;

public class Student 
implements StudentInterface 
{
    private Integer           studentId_; 
    private String            firstName_;
    private String            lastName_;
    private StudentRecordList studentRecordList_;

    
    
    public Student(Integer studentId, String firstName,
                    String lastName, StudentRecordList studentRecordList) 
    { 
        boolean recordlistIsNull = studentRecordList == null;
        if (recordlistIsNull) {
            studentRecordList_ = new StudentRecordList();
        }
        
        studentRecordList_ = studentRecordList;
        studentId_         = studentId; 
        firstName_         = firstName;
        lastName_          = lastName;
    }


        
    public Integer getStudentId() 
    { 
        return studentId_;
    } 
    
    
    
    public String getFirstName() 
    { 
        return firstName_; 
    }

   
    
    public void setFirstName(String firstName) 
    { 
        firstName_ = firstName; 
    }

    
    
    public String getLastName() 
    { 
        return lastName_; 
    }
    
    
    
    public void setLastName(String lastName) 
    { 
        lastName_ = lastName; 
    }

    
    
    public void addRecord(StudentRecordInterface studentRecord) 
    { 
        studentRecordList_.add(studentRecord);
    }
    
    
    
    public StudentRecordInterface getRecordByUnit(String unitCode) 
    {
        for (StudentRecordInterface record : studentRecordList_) {
            boolean recordIsMatch = record.getUnitCode().equals(unitCode);
            if (recordIsMatch) {
                return record; 
            }
        }
        return null;       
    }

    
    
    public StudentRecordList getRecords() 
    { 
        return studentRecordList_; 
    }
}

