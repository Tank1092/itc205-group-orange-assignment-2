package datamanagement;

public class UnitProxy 
implements UnitInterface 
{
    private String      unitCode_;
    private String      unitName_;
    private UnitManager unitManager_;

    
    
    public UnitProxy(String unitCode, String unitName)
    {
        unitCode_    = unitCode;
        unitName_    = unitName;
        unitManager_ = UnitManager.getInstance();
    }
    
    
    
    public String getUnitCode() 
    { 
        return unitCode_;
    }
        
    
    
    public String getUnitName() 
    { 
        return unitName_; 
    }
    
    
    
    public void setPassCutOff(float cutOff) 
    {
        unitManager_.getUnit(unitCode_).setPassCutOff(cutOff);
    }
    
    

    public float getPassCutOff() 
    {
        return unitManager_.getUnit(unitCode_).getPassCutOff();
    }
    
    
    
    public void setCreditCutOff(float cutOff) 
    {
        unitManager_.getUnit(unitCode_).setCreditCutOff(cutOff);
    }
    
    
    
    public float getCreditCutOff() 
    {
        return unitManager_.getUnit(unitCode_).getCreditCutOff();
    }
    
    

    public void setDistinctionCutOff(float cutOff) 
    {
        unitManager_.getUnit(unitCode_).setDistinctionCutOff(cutOff);
    }
    
    
    
    public float getDistinctionCutOff()
    {
        return unitManager_.getUnit(unitCode_).getDistinctionCutOff();
    }
    
    
    
    public void setHighDistinctionCutOff(float cutOff) 
    {
        unitManager_.getUnit(unitCode_).setHighDistinctionCutOff(cutOff);
    }
    
    
    
    public float getHighDistinctionCutOff() 
    {
        return unitManager_.getUnit(unitCode_).getHighDistinctionCutOff();
    }
    
    
    
    public void setAdditionalExamCutOff(float cutOff) 
    {
        unitManager_.getUnit(unitCode_).setAdditionalExamCutOff(cutOff);
    }
    
    
    
    public float getAdditionalExamCutOff() 
    {
        return unitManager_.getUnit(unitCode_).getAdditionalExamCutOff();
    }
    
    
    
    public String getGrade(float ass1, float ass2, float exam) 
    {
        return unitManager_.getUnit(unitCode_).getGrade(ass1, ass2, exam);
    }
    
    
    
    public void addStudentRecord(StudentRecordInterface record) 
    { 
        unitManager_.getUnit(unitCode_).addStudentRecord(record);
    }
    
    
    
    public StudentRecordInterface getStudentRecord(int studentId) 
    {
        return unitManager_.getUnit(unitCode_).getStudentRecord(studentId);
    }
    
    
    
    public StudentRecordList getStudentRecords() 
    {
        return unitManager_.getUnit(unitCode_).getStudentRecords();
    }
    
    
    
    public int getAsg1Weight() 
    {
        return unitManager_.getUnit(unitCode_).getAsg1Weight();
    }
    
    
    
    public int getAsg2Weight() 
    {
        return unitManager_.getUnit(unitCode_).getAsg2Weight();
    }
    
    
    
    public int getExamWeight() 
    {
        return unitManager_.getUnit(unitCode_).getExamWeight();
    }
    
    
   
    public void setAsgWeights(int asg1Weight, int asg2Weight, int examWeight) 
    {
        unitManager_.getUnit(unitCode_).setAsgWeights(asg1Weight,  asg2Weight, 
        		                                      examWeight);
    }
}

