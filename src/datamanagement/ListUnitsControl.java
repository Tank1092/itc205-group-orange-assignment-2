package datamanagement;

public class ListUnitsControl 
{
    private UnitManager unitManager_;

    
    
    public ListUnitsControl() 
    {
        unitManager_ = UnitManager.getInstance();
    }
            
    
    
    public void listUnits(UnitListerInterface lister) 
    {
        lister.clearUnits();
        UnitMap unitMap = unitManager_.getUnits();
        
        for (String student : unitMap.keySet()) {
            lister.addUnit(unitMap.get(student));
        }
    }
}