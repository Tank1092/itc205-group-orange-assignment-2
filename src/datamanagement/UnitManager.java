package datamanagement;

import java.util.List;

import org.jdom.Element;

public class UnitManager 
{	
    private static UnitManager self_ = null;
    private UnitMap            unitMap_;
    

    
    private UnitManager() 
    {
        unitMap_ = new UnitMap();
    }
    
    

    public static UnitManager getInstance() 
    {
        if (self_ == null) {
            self_ = new UnitManager();
        }
        return self_;
    }

    
    
    public UnitInterface getUnit(String unitCode) 
    {
        UnitInterface unitInterface = unitMap_.get(unitCode); 
        
        if (unitInterface != null) {
        	return unitInterface;
        }
        else {
        	return createUnit(unitCode);
        }
    }

    

    private UnitInterface createUnit(String unitCode) 
    {
        UnitInterface unitInterface;
        
        for (Element element : (List<Element>) XmlManager.getInstance().
        		                                          getDocument().
        		                                          getRootElement().
                                                          getChild("unitTable").
                                                          getChildren("unit")) {
        	boolean isUnitCode = unitCode.equals(element.
        			                             getAttributeValue("uid"));
        	
            if (isUnitCode) {
                StudentRecordList studentList;
                studentList = null;
                
                unitInterface = new Unit(element.getAttributeValue("uid"), 
                		                 element.getAttributeValue("name"),
                                         Float.valueOf(element.
                                        		       getAttributeValue("ps")).
                                                       floatValue(), 
                                         Float.valueOf(element.
                                        		       getAttributeValue("cr")).
                                                       floatValue(), 
                                         Float.valueOf(element.
                                        		       getAttributeValue("di")).
                                                       floatValue(), 
                                         Float.valueOf(element.
                                        		       getAttributeValue("hd")).
                                                       floatValue(), 
                                         Float.valueOf(element.
                                        		       getAttributeValue("ae")).
                                                       floatValue(),
                                         Integer.valueOf(element.
                                        		         getAttributeValue(
                                        		         "asg1wgt")).intValue(), 
                                         Integer.valueOf(element.
                                        		         getAttributeValue(
                                        		         "asg2wgt")).intValue(),
                                         Integer.valueOf(element.
                                        		         getAttributeValue(
                                        		         "examwgt")).intValue(), 
                                         StudentRecordManager.getInstance().
                                                              getRecordsByUnit(
                                                              unitCode));
               
                unitMap_.put(unitInterface.getUnitCode(), unitInterface);
                
                return unitInterface;
            }
        }

        throw new RuntimeException("DBMD: createUnit : unit not in file");
    }
    
    

    public UnitMap getUnits() 
    {
        UnitMap unitMap = new UnitMap();
        UnitInterface unitInterface;
        
        for (Element element : (List<Element>) XmlManager.getInstance().
        		                                          getDocument().
        		                                          getRootElement().
                                                          getChild("unitTable").
                                                          getChildren("unit")) {
            unitInterface = new UnitProxy(element.getAttributeValue("uid"), 
            		                      element.getAttributeValue("name"));
            unitMap.put(unitInterface.getUnitCode(), unitInterface);
        } // unit maps are filled with PROXY units
        
        return unitMap;
    }
}
