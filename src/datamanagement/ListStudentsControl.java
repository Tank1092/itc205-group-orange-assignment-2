package datamanagement;
      
public class ListStudentsControl 
{
    private StudentManager studentManager_;

    
    
    public ListStudentsControl() 
    {
        studentManager_ = StudentManager.getInstance();
    }
    
    

    public void listStudents(StudentListerInterface lister, String unitCode) 
    {
        lister.clearStudents();
        StudentMap studentMap = studentManager_.getStudentsByUnit(unitCode);
        
        for (Integer studentId : studentMap.keySet()) {
            lister.addStudent(studentMap.get(studentId));
        }   
    }    
}