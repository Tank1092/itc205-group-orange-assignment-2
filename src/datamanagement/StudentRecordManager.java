package datamanagement;

import java.util.List;
import org.jdom.Element;

public class StudentRecordManager 
{
    private static StudentRecordManager self_ = null;
    private StudentRecordMap recordMap_;
    private java.util.HashMap<String, StudentRecordList> unitRecordMap_;
    private java.util.HashMap<Integer, StudentRecordList> studentRecordMap_;

    
    
    public static StudentRecordManager getInstance() 
    {
        if (self_ == null)
            self_ = new StudentRecordManager();
        return self_;
    }

    
    
    private StudentRecordManager() 
    {
        recordMap_ = new StudentRecordMap();
        unitRecordMap_ = new java.util.HashMap<>();
        studentRecordMap_ = new java.util.HashMap<>();
    }

    
    
    public StudentRecordInterface getStudentRecord(Integer studentID,
                                                   String unitCode) 
    {
        StudentRecordInterface recordInterface = recordMap_.get(studentID.
                                                                toString() + 
                                                                unitCode);
        return recordInterface != null ? recordInterface : 
                                  createStudentRecord(studentID, unitCode);
    }

    
    
    private StudentRecordInterface createStudentRecord(Integer studentIdKey,
                                                       String unitIdKey) 
    {
        StudentRecordInterface recordInterface;
        List<Element> list = (List<Element>) XmlManager.getInstance().
                                             getDocument().
                                             getRootElement().
                                             getChild("studentUnitRecordTable").
                                             getChildren("record");
        
        // Search for the entry in the database that matches the arguments and 
        // load as a StudentRecord object
        for (Element element : list) {
            boolean keyMatchesSid = studentIdKey.toString().
                                    equals(element.getAttributeValue("sid"));
            
            boolean keyMatchesUid = unitIdKey.equals(element.
                                    getAttributeValue("uid"));
            
            if (keyMatchesSid && keyMatchesUid) 
            {
                Integer studentId = new Integer(element.getAttributeValue(
                                    "sid"));    
                String  unitId   = element.getAttributeValue("uid");    
                
                Float   asg1Mark = new Float(element.getAttributeValue("asg1")).
                                   floatValue();
                Float   asg2Mark = new Float(element.getAttributeValue("asg2")).
                                   floatValue();
                Float   examMark = new Float(element.getAttributeValue("exam")).
                                   floatValue();
                
                recordInterface  = new StudentRecord(studentId, 
                                                     unitId, 
                                                     asg1Mark, 
                                                     asg2Mark, 
                                                     examMark);
                
                recordMap_.put(recordInterface.getStudentId().toString() + 
                               recordInterface.getUnitCode(), recordInterface);
                
                return recordInterface;
            }
        }
        throw new RuntimeException(
                  "DBMD: createStudent : student unit record not in file");
    }

    
    
    public StudentRecordList getRecordsByUnit(String unitIdKey) 
    {
        StudentRecordList records = unitRecordMap_.get(unitIdKey);
        
        if (records != null)
        {
            return records;
        } 
        else {
            records = new StudentRecordList();
        }
        
        List<Element> list = (List<Element>) XmlManager.getInstance().
                                             getDocument().
                                             getRootElement().
                                             getChild("studentUnitRecordTable").
                                             getChildren("record");
        
        for (Element element : list) {
            boolean keyMatchesUid = unitIdKey.
                                    equals(element.getAttributeValue("uid"));
            
            if (keyMatchesUid)
            {
                records.add(new StudentRecordProxy(
                            new Integer(element.getAttributeValue("sid")),
                            element.getAttributeValue("uid")));
            }
        }
        
        if (records.size() > 0)
        {
            // Warning - could be empty
            unitRecordMap_.put(unitIdKey, records); 
        }
        
        return records; 
    }

    
    
    public StudentRecordList getRecordsByStudent(Integer studentIdKey) 
    {
        StudentRecordList records = studentRecordMap_.get(studentIdKey);
        
        if (records != null)
        {
            return records;
        }
        else {
            records = new StudentRecordList();  
        }
     
        List<Element> list = (List<Element>) XmlManager.getInstance().
                                             getDocument().
                                             getRootElement().
                                             getChild("studentUnitRecordTable").
                                             getChildren("record");
        
        for (Element element : list) {
            boolean keyMatchesSid = studentIdKey.toString().
                                    equals(element.getAttributeValue("sid"));
            
            if (keyMatchesSid)
            {
                records.add(new StudentRecordProxy(
                            new Integer(element.getAttributeValue("sid")),
                            element.getAttributeValue("uid")));
            }
        }
        
        if (records.size() > 0)
        {
            // Warning - could be empty
            studentRecordMap_.put(studentIdKey, records); 
        }
        
        return records;
    }

    
    
    // Search for the entry in the database that matches the unit code ID and  
    // the student ID of the argument and update the database values
    public void saveRecord(StudentRecordInterface recordInterface) 
    { 
        List<Element> list = (List<Element>) XmlManager.getInstance().
                                             getDocument().
                                             getRootElement().
                                             getChild("studentUnitRecordTable").
                                             getChildren("record");
        
        for (Element element : list) {
            boolean studentIdMatchesSid = recordInterface.getStudentId().
                                          toString().equals(
                                          element.getAttributeValue("sid"));
            
            boolean unitIdMatchesUid    = recordInterface.getUnitCode().equals(
                                          element.getAttributeValue("uid"));
            
            if (studentIdMatchesSid && unitIdMatchesUid) 
            {
                String asg1Mark = Float.toString(recordInterface.getAsg1Mark());
                String asg2Mark = Float.toString(recordInterface.getAsg2Mark());
                String examMark = Float.toString(recordInterface.getExamMark());
                
                element.setAttribute("asg1", asg1Mark);
                element.setAttribute("asg2", asg2Mark);
                element.setAttribute("exam", examMark);
                XmlManager.getInstance().saveDocument(); // write out the XML 
                                                         // file for continuous
                                                         // save
                return;
            }
        }

        throw new RuntimeException(
                "DBMD: saveRecord : no such student record in data");
    }
}
