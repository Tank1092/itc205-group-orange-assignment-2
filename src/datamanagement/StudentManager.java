package datamanagement;

import org.jdom.Element;

import java.util.List;

public class StudentManager 
{
    private static StudentManager                 self_ = null;
    private StudentMap                            studentMap_;
    private java.util.HashMap<String, StudentMap> unitMap_;
    

    
    private StudentManager() 
    {
        studentMap_ = new StudentMap();
        unitMap_ = new java.util.HashMap<>();
    }
    
    
    
    public static StudentManager getInstance() 
    {
        boolean selfIsNull = self_ == null;
        if (selfIsNull) {
            self_ = new StudentManager();
        }
        
        return self_;
    }


    
    public StudentInterface getStudent(Integer studentId) 
    {
        StudentInterface studentInterface = studentMap_.get(studentId);
        
        if (studentInterface != null) {
            return studentInterface;
        }   
        
        return createStudent(studentId);
    }

    
    
    private Element getStudentElement(Integer studentId) 
    {       
        @SuppressWarnings("unchecked")
        List<Element> elements = XmlManager.getInstance().getDocument().
                                 getRootElement().getChild("studentTable").
                                 getChildren("student");
 
        for (Element element : elements) {
            boolean studentIdIsMatching = studentId.toString().
                                          equals(element.
                                          getAttributeValue("sid"));
            
            if (studentIdIsMatching) {
                return element;
            }
        }
        
        return null;
    }


    
    private StudentInterface createStudent(Integer studentId) 
    {
        StudentInterface studentInterface;
        Element element = getStudentElement(studentId);
        
        if (element != null) {
            Integer studentIdNumber = new Integer(element.
                                      getAttributeValue("sid"));
            String  firstName = element.getAttributeValue("fname");
            String  lastName  = element.getAttributeValue("lname");
            
            StudentRecordList records = StudentRecordManager.getInstance().
                                        getRecordsByStudent(studentId);

            studentInterface = new Student(studentIdNumber, 
                                           firstName,
                                           lastName,
                                           records);
            
            studentMap_.put(studentInterface.getStudentId(), studentInterface);
            
            return studentInterface;
        }
        
        throw new RuntimeException("DBMD: createStudent : student not in file");        
    }

    

    private StudentInterface createStudentProxy(Integer studentId) 
    {   
        Element element = getStudentElement(studentId);
        
        if (element != null) {
            String firstName = element.getAttributeValue("fname");
            String lastName  = element.getAttributeValue("lname");
            
            return new StudentProxy(studentId, firstName, lastName);
        }
        
        throw new RuntimeException("DBMD: createStudent : student not in file");
    }

    
    
    public StudentMap getStudentsByUnit(String unitCode) 
    {
        StudentMap studentMap = unitMap_.get(unitCode);
        
        if (studentMap != null) {
            return studentMap;
        }

        studentMap = new StudentMap();
        StudentInterface studentInterface;
        StudentRecordList records = StudentRecordManager.getInstance().
                                    getRecordsByUnit(unitCode);
        
        for (StudentRecordInterface record : records) {
             studentInterface = createStudentProxy(new Integer(record.
                                                   getStudentId()));
             studentMap.put(studentInterface.getStudentId(), studentInterface);
        }
        
        unitMap_.put(unitCode, studentMap);
        return studentMap;
    }
}

