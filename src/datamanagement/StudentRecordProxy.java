package datamanagement;

public class StudentRecordProxy 
implements StudentRecordInterface 
{
    private Integer studentId_;
    private String unitCode_;
    private StudentRecordManager recordManager_;

    
    
    public StudentRecordProxy(Integer studentId, String unitCode) 
    {
        studentId_ = studentId;
        unitCode_ = unitCode;
        recordManager_ = StudentRecordManager.getInstance();
    }

    
    
    public Integer getStudentId() 
    {
        return studentId_;
    }

    
    
    public String getUnitCode() 
    {
        return unitCode_;
    }

    
    
    public void setAsg1Mark(float mark) 
    {
        recordManager_.getStudentRecord(studentId_, unitCode_).
        setAsg1Mark(mark);
    }

    
    
    public float getAsg1Mark() 
    {
        return recordManager_.getStudentRecord(studentId_, unitCode_).
               getAsg1Mark();
    }

    
    
    public void setAsg2Mark(float mark) 
    {
        recordManager_.getStudentRecord(studentId_, unitCode_).
        setAsg2Mark(mark);
    }

    
    
    public float getAsg2Mark() 
    {
        return recordManager_.getStudentRecord(studentId_, unitCode_).
               getAsg2Mark();
    }

    
    
    public void setExamMark(float mark) 
    {
        recordManager_.getStudentRecord(studentId_, unitCode_).
        setExamMark(mark);
    }

    
    
    public float getExamMark() 
    {
        return recordManager_.getStudentRecord(studentId_, unitCode_).
               getExamMark();
    }

    
    
    public float computeTotalMark() 
    {
        return recordManager_.getStudentRecord(studentId_, unitCode_).
               computeTotalMark();
    }
}
